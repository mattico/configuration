$env:Path = $env:Path + "C:\tools\cmder\vendor\msysgit\bin";

# Load posh-git example profile
. 'C:\bin\poshgit\dahlbyk-posh-git-869d4c5\profile.example.ps1'

# Load Jump-Location profile
Import-Module 'C:\ProgramData\chocolatey\lib\Jump-Location.0.6.0\tools\Jump.Location.psd1'

# Set up our prompt.
function global:prompt {
    $realLASTEXITCODE = $LASTEXITCODE
    # Reset color, which can be messed up by Enable-GitColors
    $Host.UI.RawUI.ForegroundColor = $GitPromptSettings.DefaultForegroundColor
    Write-Host($pwd.ProviderPath) -nonewline -ForegroundColor Green
    Write-VcsStatus
    # Prompt on newline, with cmder colours.
    Write-Host
    Write-Host ">" -nonewline -ForegroundColor DarkGray
    $global:LASTEXITCODE = $realLASTEXITCODE
    return " "
}
# More posh-git init.
Enable-GitColors
Start-SshAgent -Quiet

# change to unicode Character Page to allow AirLine
chcp 65001
